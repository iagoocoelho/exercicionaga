function changeStyle(style)
{
  var changer = document.getElementById("changer");

  if (style.id == 'botao1') {
    changer.classList.add("changer1");
    changer.classList.remove("changer2");
  }

  if(style.id == 'botao2') {
    changer.classList.add("changer2")
    changer.classList.remove("changer1")
  }

  if(style.id == 'botao3'){
    changer.classList.remove("changer1");
    changer.classList.remove("changer2");
  }
}